package com.laboratorio12.stream.service;

import io.micrometer.core.instrument.util.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

@Service
public class StreamService {

    public static final Charset CHARSET = Charset.forName("UTF-8");

    public StreamService () {}


    public File inverseFile (String filename, String outputFileName) throws IOException {
        File outputFile = new File(outputFileName);
        FileInputStream in = new FileInputStream(filename);
        OutputStream out = new FileOutputStream(outputFile);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String strLine;
        Stack<String> stack = new Stack<>();
        while ((strLine = br.readLine()) != null) {
            stack.push(strLine);
        }
        br.close();
        String reversed = "";
        while (!stack.isEmpty()) {
            String letter = stack.pop();
            reversed += letter+"\n";
        }
        StreamUtils.copy(reversed, StandardCharsets.UTF_8, out);
        in.close();
        out.close();
        return outputFile;
    }

    public int countSongVerses (String filename) throws IOException {
        int count = 0;
        FileInputStream in = new FileInputStream(filename);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String strLine;
        String strLineAux = "";
        while ((strLine = br.readLine()) != null) {
            strLineAux = strLine;
            if (strLine.equals("")) {
                count++;
            }
        }
        br.close();
        in.close();
        if (!strLineAux.equals("")) {
            count++;
        }
        return count;
    }

    public Map<String, Integer> getMostRepeatedWord (String filename) throws IOException {
        String mostRepeatedWord = "";
        int repeticiones = 0;
        String fileContent = getString(filename);
        HashMap<String, Integer> ocurrences = new HashMap<>();
        String fileContentCleaned = fileContent.replaceAll(",", "").replaceAll("\n", " ").toLowerCase();
        String[] parts = fileContentCleaned.split(" ");
        for (String part: parts) {
            if (ocurrences.containsKey(normalize(part))) {
                Integer integer = ocurrences.get(normalize(part));
                ocurrences.replace(normalize(part), ++integer);
            } else {
                ocurrences.putIfAbsent(normalize(part), 1);
            }
        }
        for (Map.Entry<String, Integer> entry : ocurrences.entrySet()) {
            String key = entry.getKey();
            Integer i = entry.getValue();
            if (repeticiones < i) {
                repeticiones = i;
                mostRepeatedWord = key;
            }
        }
        Map<String, Integer> map = new HashMap<>();
        map.put(mostRepeatedWord, repeticiones);
        return map;
    }

    public File generateStatitics (String filename, String statiticsFilename) throws IOException {
        int versesNumber = countSongVerses(filename);
        Map<String, Integer> result = getMostRepeatedWord(filename);
        String mostRepeatedWord = result.keySet().stream().findFirst().get();
        Integer numberOfRepetitions = result.get(mostRepeatedWord);

        File statiticsFile = new File(statiticsFilename);
        OutputStream out = new FileOutputStream(statiticsFile);
        String text = String.format("Estadísticas:\nPalabra más repetida: %s\nCantidad de veces: %s\nNúmero de estrofas: %s",
                mostRepeatedWord, numberOfRepetitions, versesNumber);
        StreamUtils.copy(text, StandardCharsets.UTF_8, out);
        out.close();
        return statiticsFile;
    }

    public File generateOutputFinal (String filename, String outputFilename) throws IOException {
        Map<String, Integer> map = getMostRepeatedWord(filename);
        String mostRepeatedWord = map.keySet().stream().findFirst().get();

        File finalOutputFile = new File(outputFilename);
        OutputStream finalOut = new FileOutputStream(finalOutputFile);
        String fileContentToFinal = getString(filename);
        String textFinal = fileContentToFinal.replaceAll(mostRepeatedWord, "you");
        StreamUtils.copy(textFinal, StandardCharsets.UTF_8, finalOut);
        finalOut.close();
        return finalOutputFile;
    }

    private String normalize (String word) {
        return word.toLowerCase();
    }

    public String getString(InputStream input) throws IOException {
        return IOUtils.toString(input, CHARSET);
    }

    public String getString(String file) throws IOException {
        InputStream in = new FileInputStream(file);
        return getString(in);
    }
}
