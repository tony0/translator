package com.laboratorio12.stream.service;

import org.springframework.boot.test.context.SpringBootTest;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import static org.testng.Assert.*;

@SpringBootTest
public class StreamServiceTest {

    public static final String MOST_REPEATED_WORD_ORIGINAL = "beggin'";
    public static final int REPEATED_TIMES = 27;
    public static final String YOU = "you";
    private String PATH = "yourpath";

    public final String ORIGINAL = PATH + "Original.txt";
    public final String ESTROFAS_EN_ORDEN_INVERSO = PATH+"estrofasEnOrdenInverso.txt";
    public final String STATITICS = PATH+"statitics.txt";
    public final String FINAL = PATH+"finaloutput.txt";

    public static final int VERSES_SONG = 17;

    private StreamService streamService;

    public StreamServiceTest () {
        this.streamService = new StreamService();
    }

    @Test
    void testInverseFile () throws IOException {
        File file = streamService.inverseFile(ORIGINAL, ESTROFAS_EN_ORDEN_INVERSO);
        assertTrue(file.exists());
        String original = streamService.getString(ORIGINAL);
        String reverso = streamService.getString(ESTROFAS_EN_ORDEN_INVERSO);
        assertNotEquals(original, reverso);
    }

    @Test
    void testCountVerses () throws IOException {
        int versesNumber = streamService.countSongVerses(ORIGINAL);
        assertEquals(versesNumber, VERSES_SONG);
    }

    @Test
    void testMostRepeaterWord () throws IOException {
        Map<String, Integer> map = streamService.getMostRepeatedWord(ESTROFAS_EN_ORDEN_INVERSO);
        String mostRepeatedWord = map.keySet().stream().findFirst().get();
        Integer numberOfRepetitions = map.get(mostRepeatedWord);
        assertEquals(mostRepeatedWord, MOST_REPEATED_WORD_ORIGINAL);
        assertEquals(numberOfRepetitions.intValue(), REPEATED_TIMES);
    }

    @Test
    void testStatitics () throws IOException {
        File f = streamService.generateStatitics(ESTROFAS_EN_ORDEN_INVERSO, STATITICS);
        assertTrue(f.exists());
    }

    @Test
    void testFinalOutput () throws IOException {
        File f = streamService.generateOutputFinal(ESTROFAS_EN_ORDEN_INVERSO, FINAL);
        assertTrue(f.exists());
        Map<String, Integer> map = streamService.getMostRepeatedWord(FINAL);
        String mostRepeatedWord = map.keySet().stream().findFirst().get();
        assertEquals(mostRepeatedWord, YOU);
    }
}